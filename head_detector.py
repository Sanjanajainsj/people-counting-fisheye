import cv2

import sys
sys.path.append('darknet')
import darknet

from config import config as _config

class HeadDetector:
    def __init__(self, config=_config["head_detector"]):
        self.config = config
        self.net = darknet.load_net(self.config["config_path"], self.config["weights_path"],0)
        self.meta = darknet.load_meta(self.config["meta_path"])
        
    def _detect_frame(self, frame):
        """
            Detect a single frame
            Args:
                frame: a numpy array of the frame
            Returns: 
                A list of head boxes (xmin, ymin, xmax, ymax, <CLASS_ID_OF_HEAD>)
        """
        results = []
        boxes = darknet.detect_np(self.net, self.meta, frame, thresh=self.config["confidence_threshold"], nms=self.config["nms_threshold"])
        for r in boxes:
            name = r[0]
            predict = r[1]
            x = r[2][0]
            y = r[2][1]
            w = r[2][2]
            z = r[2][3]
            w = max(r[2][2],r[2][3])
            z = max(r[2][2],r[2][3])
            w=50
            z=50
            x_max = (2*x+w)/2
            x_min = (2*x-w)/2
            y_min = (2*y-z)/2
            y_max = (2*y+z)/2
            results.append([x_min, y_min, x_max, y_max, 1]) # 1 is class id of head
        return results
    
    def _mask_frame(self, frame, masks):
        """
            Mask a frame
            Args:
                frame: a numpy array of the frame
                masks: a list of mask (xmin, ymin, xmax, ymax)
            Returns:
                a masked frame
        """
        for mask in masks:
            frame[mask[0]:mask[2], mask[1]:mask[3]] = 0
        return frame
    
    def detect(self, video_path):
        """
            Given a video path, return a list of frame detections
            Args:
                video_path: path to the video
            Returns:
                A list of frame detections (a list of boxes, frame number)
        """
        cap = cv2.VideoCapture(video_path)
        detections = []
        while True:
            ret, frame = cap.read()
            frame_no = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
            if ret:
                frame = self._mask_frame(frame, self.config["frame_masks"])
                results = self._detect_frame(frame)
                detections.append([results, frame_no])
            else:
                break
        cap.release()
        return detections
            