import numpy as np
import sys

sys.path.append('sort-master')
from sort import Sort

from config import config as _config

class Tracker:
    def __init__(self, config=_config["head_tracker"]):
        self.config = config
        self.head_tracker = Sort(max_age=self.config["maxage"], min_hits=self.config["minhit"])

    def _update_track(self, head_frame_list, iou_thresh):
        track_ids = self.head_tracker.update(head_frame_list, iou_thresh)
        return track_ids
        
    def get_person_list(self, head_detection_list):
        """
            Given the head detection list, runs the tracker and returns a person_list
            Args:
                head_detection_list: head detection list returned from the detection module
            Returns:
                A list of person boxes in the following format: person_list=list([x1,y1,x2,y2,id,frameno],...)
        """
        person_list=[]

        for frame in head_detection_list:
            head_frame_list=frame[0]
            frame_no=int(frame[1])
            head_frame_arr = np.array(head_frame_list)
            track_ids = self._update_track(head_frame_arr,self.config["iou_thresh"])
            [person_list.append([person_box[0],person_box[1],person_box[2],person_box[3],person_box[4], frame_no]) for person_box in track_ids]

        return person_list

    def get_track_list(self, person_list):
        """
            Given the person_list, groups the tracks and returns a track_list
            Args:
                person_list: list of person boxes in the following format: person_list=list([x1,y1,x2,y2,id,frameno],...)
            Returns:
                A list of grouped tracks in the following format: track_list=list([person_box1_id1, person_box2_id1, person_box3_id1], [person_box1_id2, person_box2_id2, ..], ...)
        """
        id_list = list(); [id_list.append(person_box[4]) for person_box in person_list]
        uniq_id_list = set(id_list)
        track_list = list()
        for ids in uniq_id_list:
            track = list()
            for person_box in person_list:
                if person_box[4] == ids: track.append(person_box)
            track_list.append(track)
        return track_list

            
