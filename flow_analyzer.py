from head_detector import HeadDetector
from tracker import Tracker
from head_counter import HeadCounter

from config import config as _config
import pickle

class FlowAnalyzer:
    def __init__(self, config=_config):
        self.config = config
        self.head_detector = HeadDetector()
        self.tracker = Tracker()
        self.head_counter = HeadCounter()
        
    def analyze(self, video_path):
        # print("Start detection\n")
        detections = self.head_detector.detect(video_path)
        # print(detections)
        person_list = self.tracker.get_person_list(detections)
        # print(person_list)
        track_list = self.tracker.get_track_list(person_list)
        # print(track_list)
        results = self.head_counter.get_count(track_list)
        # return results
