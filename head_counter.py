import math
import numpy as np

from config import config as _config

class HeadCounter:
    def __init__(self, config=_config["head_counter"]):
        self.config = config

    def _select_track_border(self, track_list, enter_or_exit, P1, P2, angle, location, y_region_crossed=0.5):
        '''
        take in track_list, select tracks that enter or exit from the specified virtual border
        '''
        exit_track_list = list()
        
        for track in track_list:
            
            y_mid_list = list()
            x_mid_list = list()
            frameno_list = list()
            [y_mid_list.append(person_box[3]+((person_box[1] - person_box[3])*y_region_crossed)) for person_box in track]
            [x_mid_list.append((person_box[0] + person_box[2])/2) for person_box in track]
            [frameno_list.append(person_box[5]) for person_box in track]
            
            y_mid_first = y_mid_list[0]
            x_mid_first = x_mid_list[0]
            y_mid_last = y_mid_list[-1]
            x_mid_last = x_mid_list[-1]
            
            y_mid_first_gt = P1[1] + ((x_mid_list[0]-P1[0]) * math.tan(angle * np.pi / 180.0))
            x_mid_first_gt = P1[0] + ((y_mid_list[0]-P1[1])/(math.tan(angle * np.pi / 180.0)))
            y_mid_last_gt = P1[1] + ((x_mid_list[-1]-P1[0]) * math.tan(angle * np.pi / 180.0))
            x_mid_last_gt = P1[0] + ((y_mid_list[-1]-P1[1])/ (math.tan(angle * np.pi / 180.0)))
            
            min_y_mid_gt = min(P1[1],P2[1])
            min_x_mid_gt = min(P1[0],P2[0])
            max_y_mid_gt = max(P1[1],P2[1])
            max_x_mid_gt = max(P1[0],P2[0])
            
            if enter_or_exit == "Enter":
                
                if location=="bottom":
                    if y_mid_first > y_mid_first_gt and x_mid_first > min_x_mid_gt and x_mid_first < max_x_mid_gt:
                        if y_mid_last < y_mid_last_gt or x_mid_last < min_x_mid_gt or x_mid_last > max_x_mid_gt:
                            if frameno_list[0] < frameno_list[-1]:
                                exit_track_list.append(track)
                elif location=="top":
                    if y_mid_first < y_mid_first_gt and x_mid_first > min_x_mid_gt and x_mid_first < max_x_mid_gt:
                        if y_mid_last > y_mid_last_gt or x_mid_last < min_x_mid_gt or x_mid_last > max_x_mid_gt:
                            if frameno_list[0] < frameno_list[-1]:
                                exit_track_list.append(track)
                elif location=="right":
                    if x_mid_first > x_mid_first_gt and y_mid_first > min_y_mid_gt and y_mid_first < max_y_mid_gt:
                        if x_mid_last < x_mid_last_gt or y_mid_last < min_y_mid_gt or y_mid_last > max_y_mid_gt:
                            if frameno_list[0] < frameno_list[-1]:
                                exit_track_list.append(track)
                elif location=="left":
                    if x_mid_first < x_mid_first_gt and y_mid_first > min_y_mid_gt and y_mid_first < max_y_mid_gt:
                        if x_mid_last > x_mid_last_gt or y_mid_last < min_y_mid_gt or y_mid_last > max_y_mid_gt:
                            if frameno_list[0] < frameno_list[-1]:
                                exit_track_list.append(track)
                                
            elif enter_or_exit == "Exit":
                
                if location=="top":
                    if y_mid_last < y_mid_last_gt and x_mid_last > min_x_mid_gt and x_mid_last < max_x_mid_gt:
                        exit_track_list.append(track)
                elif location=="bottom":
                    if y_mid_last > y_mid_last_gt and x_mid_last > min_x_mid_gt and x_mid_last < max_x_mid_gt:
                        exit_track_list.append(track)
                elif location=="right":
                    if x_mid_last > x_mid_last_gt and y_mid_last > min_y_mid_gt and y_mid_last < max_y_mid_gt:
                        exit_track_list.append(track)
                elif location=="left":
                    if x_mid_last < x_mid_last_gt and y_mid_last > min_y_mid_gt and y_mid_last < max_y_mid_gt:
                        exit_track_list.append(track)

        return exit_track_list

    def _check_exit(self, track, P1,P2,angle,location,region_crossed=0.5):
        '''
        takes in a track, checks if the track's last location is inside any of the virtual borders
        '''
        
        y_mid_list = list()
        x_mid_list = list()
        frameno_list = list()
        [y_mid_list.append(person_box[3]+((person_box[1] - person_box[3])*region_crossed)) for person_box in track]
        [x_mid_list.append((person_box[0] + person_box[2])/2) for person_box in track]
        [frameno_list.append(person_box[5]) for person_box in track]
        
        y_mid_last = y_mid_list[-1]
        x_mid_last = x_mid_list[-1]

        y_mid_last_gt = P1[1] + ((x_mid_list[-1]-P1[0]) * math.tan(angle * np.pi / 180.0))
        x_mid_last_gt = P1[0] + ((y_mid_list[-1]-P1[1])/ (math.tan(angle * np.pi / 180.0)))

        min_y_mid_gt = min(P1[1],P2[1])
        min_x_mid_gt = min(P1[0],P2[0])
        max_y_mid_gt = max(P1[1],P2[1])
        max_x_mid_gt = max(P1[0],P2[0])    
        
        if location=="top":
            if y_mid_last < y_mid_last_gt and x_mid_last > min_x_mid_gt and x_mid_last < max_x_mid_gt:
                return True
        elif location=="bottom":
            if y_mid_last > y_mid_last_gt and x_mid_last > min_x_mid_gt and x_mid_last < max_x_mid_gt:
                return True
        elif location=="right":
            if x_mid_last > x_mid_last_gt and y_mid_last > min_y_mid_gt and y_mid_last < max_y_mid_gt:
                return True
        elif location=="left":
            if x_mid_last < x_mid_last_gt and y_mid_last > min_y_mid_gt and y_mid_last < max_y_mid_gt:
                return True
        return False

                
    def _create_new_tracks(self,track, idx, track_new_no):
        
        '''
        takes in a track, and divides it into two based on the index specified
        '''
        track_old=track[:idx+1]
        track_new=track[idx+1:]
        for j in track_new:
            j[4]=track_new_no
        track_new_no += 1    
        self.updated_track_list.append(track_old)
        self.updated_track_list.append(track_new)
        return track_new_no

    def _check_track_end(self,track_list,x_left_mask,x_right_mask,y_region_crossed=0.5):

        '''
        Takes in a track_list, and checks if is has crossed the masked end. 
        If so it creates a new track on the track's return from the masked region
        '''
        self.updated_track_list = list()
        
        for track in track_list:
            
            y_mid_list = list()
            x_mid_list = list()
            frameno_list = list()
            trackno_list = list()
            [y_mid_list.append(person_box[3]+((person_box[1] - person_box[3])*y_region_crossed)) for person_box in track]
            [x_mid_list.append((person_box[0] + person_box[2])/2) for person_box in track]
            [frameno_list.append(person_box[5]) for person_box in track]
            [trackno_list.append(person_box[4]) for person_box in track]
            
            track_new_no=max(trackno_list)+1.0
            min_x_idx = np.argmin(np.array(x_mid_list))
            max_x_idx = np.argmax(np.array(x_mid_list))
            
            keep_track=True
            if x_mid_list[0]>x_left_mask:
                if min(x_mid_list)<x_left_mask:
                    x_check_mid_list=x_mid_list[min_x_idx+1:]
                    if len(x_check_mid_list)>0:
                        if max(x_check_mid_list)>x_left_mask:
                            keep_track=False
                            track_new_no=self._create_new_tracks(track,min_x_idx,track_new_no)
            if keep_track:
                if x_mid_list[0]<x_right_mask:
                    if max(x_mid_list)>x_right_mask:
                        x_check_mid_list=x_mid_list[max_x_idx+1:]
                        if len(x_check_mid_list)>0:
                            if min(x_check_mid_list)<x_right_mask:
                                keep_track=False
                                track_new_no=self._create_new_tracks(track,max_x_idx,track_new_no)
            if keep_track:
                self.updated_track_list.append(track)
                
        return self.updated_track_list

    def _det(self, a, b):
        return a[0] * b[1] - a[1] * b[0]

    def _line_intersection(self, line1, line2):
        
        xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
        ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

        div = self._det(xdiff, ydiff)
        if div == 0:
            return False 

        d = (self._det(*line1), self._det(*line2))
        x = self._det(d, xdiff) / div
        y = self._det(d, ydiff) / div
        
        if x<max(line2[0][0],line2[1][0]) and x>min(line2[0][0],line2[1][0]) and y<max(line2[0][1],line2[1][1]) and y>min(line2[0][1],line2[1][1]):
            if x<max(line1[0][0],line1[1][0]) and x>min(line1[0][0],line1[1][0]) and y<max(line1[0][1],line1[1][1]) and y>min(line1[0][1],line1[1][1]):
                return True
            else:
                return False
        else:
            return False

    def _compute_edge(self, prev_selected, P2_sel):

        P1_sel=((prev_selected[0] + prev_selected[2])/2,(prev_selected[1] + prev_selected[3])/2)

        y_diff=(P2_sel[1]-P1_sel[1])
        x_diff=(P2_sel[0]-P1_sel[0])
        angle_sel = math.atan(y_diff/x_diff) * 180.0 / np.pi
        P_edge=-1

        if abs(x_diff)>abs(y_diff):
            if P2_sel[0]<=P1_sel[0]:
                P_edge = (1,P1_sel[1] + ((1-P1_sel[0])*(math.tan(angle_sel * np.pi / 180.0))))
            else:
                P_edge = (1280,P1_sel[1] + ((1280-P1_sel[0])*(math.tan(angle_sel * np.pi / 180.0))))
        else:
            if P2_sel[1]<=P1_sel[1]:
                P_edge = (P1_sel[0] + ((1-P1_sel[1])/(math.tan(angle_sel * np.pi / 180.0))),1)
            else:
                P_edge = (P1_sel[0] + ((1280-P1_sel[1])/(math.tan(angle_sel * np.pi / 180.0))),1280)

        return P_edge


        

    def get_count(self,track_list):
        
        #loads the config file hyper-parameters

        borders = self.config["borders"]
        track_size_list = self.config["track_size_list"]
        min_dist = self.config["min_track_dist"]
        x_left_mask = self.config["x_left_mask"]
        x_right_mask = self.config["x_right_mask"]
        exit_borders_order = self.config["exit_borders_order"]
        
        
        #track_new_list=track_list
        track_new_list = self._check_track_end(track_list,x_left_mask,x_right_mask)
        
        # Loads the entrance border info

        P1_entrance = borders["Entrance"]["P1"]
        angle_entrance = borders["Entrance"]["angle"]
        width_entrance = borders["Entrance"]["width"]
        location_entrance = borders["Entrance"]["location"]
        P2_entrance=(P1_entrance[0]+width_entrance,int(P1_entrance[1] + width_entrance * math.tan(angle_entrance * np.pi / 180.0)))
        track_entrance = self._select_track_border(track_new_list, "Enter", P1_entrance, P2_entrance, angle_entrance, location_entrance)
        count_entrance = len(track_entrance)
        
        # Loads the exit border info into a list
        
        P1_exit = []
        angle_exit = []
        width_exit = []
        location_exit = []
        P2_exit = []
        track_exit = []
        count_exit = []
        key_list = []
        
        for border in borders["Exit"]:
            key_list.append(border["name"])
            P1 = border["P1"]
            angle = border["angle"]
            width = border["width"]
            location = border["location"]
            P2=(P1[0]+width,int(P1[1] + width * math.tan(angle * np.pi / 180.0)))
            track = self._select_track_border(track_entrance, "Exit", P1, P2, angle, location)
            P1_exit.append(P1)
            angle_exit.append(angle)
            width_exit.append(width)
            location_exit.append(location)
            P2_exit.append(P2)
            track_exit.append(track)
            count_exit.append(len(track))
        
        for track in track_entrance:
            
            check_exit_res = not self._check_exit(track, P1_entrance,P2_entrance,angle_entrance,location_entrance)
            if check_exit_res:
                for i in range(0,len(track_exit)):
                    check_exit_res = not self._check_exit(track, P1_exit[i] ,P2_exit[i], angle_exit[i], location_exit[i])
                    if not check_exit_res:
                        break;
                
            if check_exit_res:
                
                P2_sel=((track[-1][0] + track[-1][2])/2,(track[-1][1] + track[-1][3])/2) 
                last_frame_of_track=int(track[-1][5])
                prev_selected=-1
                frame_sel=-1
                
                for track_size in track_size_list:
                    if prev_selected!=-1:
                        break;
                    for j in track:
                        if int(j[5])<=last_frame_of_track-track_size:
                            P3_sel=((j[0] + j[2])/2,(j[1] + j[3])/2)
                            dist = math.hypot(P3_sel[0] - P2_sel[0], P3_sel[1] - P2_sel[1])
                            if dist>min_dist:
                                prev_selected=j
                                frame_sel=int(j[5])
                                
                if prev_selected!=-1:
                    
                    P_edge = self._compute_edge(prev_selected, P2_sel)
                    if P_edge!=-1:
                        if self._line_intersection((P2_sel,P_edge),(P1_entrance,P2_entrance)):
                            count_entrance -= 1
                        else:
                            for border_name in exit_borders_order:
                                idx = key_list.index(border_name)
                                if self._line_intersection((P2_sel,P_edge),(P1_exit[idx],P2_exit[idx])):
                                    count_exit[idx] += 1
                                    break;
        final_count={}
        final_count["Entrance"] = count_entrance
        for i in range(0,len(track_exit)):
            final_count["Exit "+key_list[i]] = count_exit[i]

        return final_count