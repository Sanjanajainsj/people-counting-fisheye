from flow_analyzer import FlowAnalyzer
from config import config as _config

import os
import boto3
import json
import io
from PIL import Image as pil_image
from IPython.display import display
from pymongo import MongoClient
import datetime

class FlowAnalyzerHandler:
    def __init__(self, config=_config, classifier=None, debug=False):
        self.config = config
        self.debug = debug
        self.sqs = boto3.client("sqs")
        self.s3 = boto3.client("s3")
        self.db = MongoClient(self.config["database"]["url"])[self.config["database"]["name"]]
        self.collection = self.db[self.config["database"]["collection"]]
        self.queue_url_input = self.config["aws"]["queue_url_input"]
        self.analyzer = FlowAnalyzer(config=self.config)
            
    def _save_to_db(self, data, filename):
        """
            Save data to the database defined in the configuration file
            Args:
                TBD
        """
        # TBD
        # save stuffs to the DB
        cam_no = int(filename.split("_")[0].split("-")[-1])
        start_timestamp = int(filename.split("_")[1].split(".")[0])
        self.collection.update_one({
                                    'cam_id':cam_no,
                                    'start_timestamp': datetime.datetime.fromtimestamp(start_timestamp)
                                  },
                                  {
                                  '$set': data
                                  },
                                  upsert = True
                                 )
        
    def start(self):
        """
            Continuously read the message from the queue.
        """
        while True:
            response = self.sqs.receive_message(
                QueueUrl = self.queue_url_input,
                MaxNumberOfMessages = 1,
                WaitTimeSeconds = 20
            )
            if not "Messages" in response:
                continue

            for message in response["Messages"]:
                body = json.loads(message["Body"])
                for record in body["Records"]:
                    object_bucket_name = record["s3"]["bucket"]["name"]
                    object_key = record["s3"]["object"]["key"]
#                     image_data = self.s3.get_object(Bucket=object_bucket_name, 
#                                                Key=object_key)
                    
                    # === YOUR MAGIC FUNCTION STARTS HERE ===

                    full_path = os.path.join(self.config["raw_video_directory"], object_key)
                    file_path, filename = os.path.split(full_path)

                    print("Downloading:", object_key)
                    if not os.path.exists(file_path):
                        os.makedirs(file_path)
                    self.s3.download_file(object_bucket_name, object_key, full_path)
                    print("Processing:", full_path)
                    
                    # Magic functions
                    results = self.analyzer.analyze(full_path)
                    print("Results: ", results)
                    
                    # Save
                    self._save_to_db(results, filename)
                    print("Saved to DB")
                    
                    # Remove file
                    os.remove(full_path)
                    
                    # ==========================================
                    
                    
                handle = message["ReceiptHandle"]
                self.sqs.delete_message(
                    QueueUrl = self.queue_url_input,
                    ReceiptHandle = handle
                )
                
if __name__ == "__main__":
    handler = FlowAnalyzerHandler(config=_config)
    handler.start()