
import cv2
import darknet

import os, sys
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import sys

sys.path.append('sort-master')


import cv2
import numpy as np
from sort import *
import pickle
from module1_selection_logic_helpers import *
from module1_counting_helpers import *


net = darknet.load_net('/home/ubuntu/workspace/darknet/cfg/obj_yolo.cfg',"/home/ubuntu/workspace/HeadDetection/darknet/backup/obj_yolo_577000.weights",0)
meta = darknet.load_meta("/home/ubuntu/workspace/darknet/cfg/obj.data")

video = sys.argv[1]
threshold = sys.argv[2]
maxage = sys.argv[3]
minhit = sys.argv[4]
iou_thresh = sys.argv[5]
video_path = "/home/ubuntu/workspace/darknet/input_videos/Event2/"+video
#save_path = "/home/ubuntu/workspace/darknet/output_videos/0.4-best-2"+video+"-"+threshold

cap = cv2.VideoCapture(video_path)
frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
total_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
print(frame_width)
print(frame_height)
fps = int(cap.get(cv2.CAP_PROP_FPS)) 
capsize = (frame_width, frame_height)
print(total_frame)
#fourcc = cv2.VideoWriter_fourcc(*'mp4v')
#out = cv2.VideoWriter(save_path,0x00000021, 7, (1280,1280))
mot_tracker = Sort(max_age= int(maxage), min_hits= int(minhit)) # default 10, 3 <<< hyper param
person_list=[]

frame_no=0
while(True):
    frame_no=int(cap.get(cv2.CAP_PROP_POS_FRAMES))
    if int(frame_no) == int(total_frame)-1:
        break;  
    ret, frame = cap.read()
    print(frame_no)
    if ret==True:
        head_list=[]
        result = darknet.detect_np(net, meta, frame,thresh=float(threshold),nms=0.4)
        for r in result:
            name = r[0]
            predict = r[1]

            x = r[2][0]
            y = r[2][1]
            w = r[2][2]
            z = r[2][3]
            w = max(r[2][2],r[2][3])
            z = max(r[2][2],r[2][3])
            w=50
            z=50
            x_max = (2*x+w)/2
            x_min = (2*x-w)/2
            y_min = (2*y-z)/2
            y_max = (2*y+z)/2
            
            #print(r[2][2],r[2][3])
            head_list.append([x_min,y_min,x_max,y_max,1])
            track_bbs_ids = mot_tracker.update(np.array(head_list),float(iou_thresh))
            [person_list.append([person_box[0],person_box[1],person_box[2],person_box[3],person_box[4], int(cap.get(1))]) for person_box in track_bbs_ids]
            cv2.rectangle(frame,(int(x_min),int(y_min)),(int(x_max),int(y_max)),(0,255,0),3)
        #out.write(frame)
#out.release()
cap.release()
cv2.destroyAllWindows()

#pickle.dump(person_list, open('/home/ubuntu/workspace/darknet/best3-person_list_trial-226_06', 'wb'))

track_list = group_tracks(person_list)


pickle.dump(track_list, open('/home/ubuntu/workspace/darknet/track-sort-result/bestmbk5trial1-577000-'+video+'-'+threshold+'-'+maxage+'-'+minhit+'-'+iou_thresh,'wb'))