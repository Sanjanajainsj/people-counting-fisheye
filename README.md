# People Counting with Head Detection

In order to run this application, the following are required:
``` 

    Python 2
    CUDA 9.0
```
You are recommended to use the AWS EC2 Deep-learning instance.

## Setup Detector

In order to setup the detector, run the following code:

```

    cd darknet
	make -f Makefile
```

## Quick Usage

Example code for getting head count for a sample video:

```

    from flow_analyzer import FlowAnalyzer
    from config import config as _config

    self.analyzer = FlowAnalyzer(config=self.config)
    results = self.analyzer.analyze(full_video_path) 
```

## Configuration

The hyperparameters can be fixed in the `config.py` file.

Value | Description
--- | ---
aws | Python dictionary of AWS information containing `queue_url_input` as the URL of the input SQS
database | Python dictionary of database information containing `url` as the URL used for connecting the database, `name` as the database name, and `collection` as the collection name.
raw_video_directory | Directory to store temporary downloaded video to be processed.
head_detector | Python dictionary for the head detector model as well as the detector hyper-parameters including the`confidence_threshold` and `nms_threshold`.
head_tracker | Python dictionary for SORT tracker hyper parameters. These include `iou_thresh`, `maxage`, and `minhit`.
head_counter | Python dictionary for hyper-parameters for counting heads. These include the following: `borders` that specify the information about the entrance and exit borders used for counting, `exit_borders_order` that specify the orders for exit border prediction of lost tracks, etc.


## Database Format and Selection Logic
Check out `_save_to_db` function inside the handler for more details.

## Model Info
The model file can be found in the following path: darknet/yolo.weights. 

Model Name | Base Model | Image Dimension | Description
--- | --- | --- | --- | --- | ---
yolo.weights | TinyYolo | (640,640) | Head detector model for Fisheye cameras

## Running in the Pipeline

Once an EC2 instance is configured, you can simply run:

```
    python flow_analyzer_handler.py
```

The handler will consume message from the SQS queue, process it, and then save the result to the database as specified in `config.py`.

If you wish to run the handler on your local machine, you will first have to create `config` and `credentials` files at your `~/.aws/` folder via AWS Command Line Interface. Please contact the owner of the repository for the access key values and other necessary information. You will also have to setup darknet on your local machine, and you should follow the following guide in order to do that.

