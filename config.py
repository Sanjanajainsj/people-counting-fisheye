config = {
    "model": {
        "path": "TBD",
        "config_path": "TBD"
    },
    "aws": {
        "model_bucket": "TBD",
        "queue_url_input": "https://sqs.ap-northeast-1.amazonaws.com/708926088643/test-head-detection-queue"
    },
    "database": {
        "url": "mongodb://testuser1234:testuser1234@ds229373.mlab.com:29373/head-detector",
        "name": "head-detector",
        "collection": "flow-analysis"
    },
    "raw_video_directory": "_temp_download",
    "head_detector": {
        "config_path": "darknet/cfg/obj_yolo.cfg",
        "weights_path": "darknet/yolo.weights",
        "meta_path": "darknet/cfg/obj.data", # names variable in this file (current: darknet/cfg/obj.names) should be relative to the root directory
        "nms_threshold": 0.4,
        "confidence_threshold": 0.29,
        "frame_masks": [ # a list of masks (xmin, ymin, xmax, ymax)
            (0, 0, 175, 1280),
            (0, 0, 225, 1280),
            (0, 1100, 1280, 1280)
        ]
    },
    "head_tracker": {
        'iou_thresh' : 0.025,
        'maxage' : 10,
        'minhit' : 3
    },

    "head_counter": {
        'borders' : { 'Entrance' : {'angle':89, 'P1':(470,370), 'width':12, 'location':'left'},
            'Exit' : [
                      {'name': 'B', 'angle':-8, 'P1':(420,870), 'width':680, 'location':'bottom'},
                      {'name': 'C', 'angle':75, 'P1':(860,290), 'width':140, 'location':'right'},
                      {'name': 'D', 'angle':4, 'P1':(256,390), 'width':650, 'location':'top'},
                     ]
         },
        'exit_borders_order': ['C','B','D'],
        'track_size_list' : [20,15,10],
        'min_track_dist' : 50.0,
        'x_left_mask' : 230,
        'x_right_mask' : 995
    },

        
}